var x = 6;
var y = 14;
var z = 4;

document.write((x += y - x++ * z) + "<br />");

//1. умножение x++ * z = 24, x++ сработает только при следующем вызове x
//2. вычетание 14 - 24 = -10
//3. присваевание x += y = -4
// x = -4

var x = 6;
var y = 14;
var z = 4;

document.write((z = --x - y * 5) + "<br />");

//1. умножение y * 5 = 70
//2. вычетание --x - y = -65, --x срабатывает сразу
//3. z = -65 

var x = 6;
var y = 14;
var z = 4;

document.write((y /= x + 5 % z) + "<br />");

//1. остаток от деления 5 % z = 1
//2. сложение x + 1 = 7
//3. присваевание y /= x = 2
//4. y = 2


var x = 6;
var y = 14;
var z = 4;

document.write((z - x++ + y * 5) + "<br />");

//1. умножение y * 5 = 70
//2. вычетание z - x++ = -2, x++ сработает только при следующем вызове x
//3. сложение x++ + y = 68

var x = 6;
var y = 14;
var z = 4;

document.write(x = y - x++ * z)

//1. умножение x++ * z = 24, x++ сработает только при следующем вызове x
//2. вычетание y - x++ = -10
//3. x = -10
