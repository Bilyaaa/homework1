console.table(countries);

function getAvaragePopulation() {
    let worldPopulation = countries.reduce(function(acc, el){
        return acc + el.population;
    }, 0);  
     let worldAvaragePopulation = worldPopulation / countries.length;
    console.log(worldAvaragePopulation);
};
getAvaragePopulation();

function getNames() {
    let countriesNames = countries.map(function(country){
        return country.name;
    })
    console.log(countriesNames);
    //вернуть список (массив) имен стран (поле name)
};
getNames();


function findCountry() {
    const countryName =  prompt("Введите название страны", "Ukraine");
    let countriesNames = countries.map(function(country){
        return country.name;
    });
    if (countriesNames.indexOf(countryName) > -1) {
        console.log("True")
    }
    else {
        console.log("False")
    };
    //ввод через prompt название страны, вернуть true если такая страна нашлась по имени, false если не нашлась. можно использовать для этого getNames выше + indexOf
}
findCountry()

function getCountryByCode() {
    let countriesA3Codes = countries.map(function(country){
        return country.alpha3Code;
    })
    const a3Code = prompt("Введите Alpha3 Code", "UKR");
    if(a3Code.length < 3 || a3Code.length > 3) {
        console.log("Неправильный ввод")
    }
    else {
        let countryIndex = countriesA3Codes.indexOf(a3Code)
        console.log(countries[countryIndex])
    }
    //пользователь вводит через prompt трехбуквенный код. если введенная строка не 3 символа длиной - выдать сообщение о неправильном вводе.
    //по введенной строке (трехбуквенный код) найти страну (должно совпадать с alpha3Code, которое есть у каждой страны).
    //и вернуть из функции либо объект с найденной страной, либо false
}
getCountryByCode()
//Удачи.